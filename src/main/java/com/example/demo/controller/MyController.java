package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

	@GetMapping("/sum")
	Integer sum(@RequestParam("a") Integer one,
					   @RequestParam("b") Integer two) {
		return one+two;
	}
	@GetMapping("/giaithua")
	Integer giaiThua(@RequestParam("n") Integer n) {
			if (n == 1) {
				return 1;
			}
			return n * giaiThua(n - 1);
	}
}
